# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

1. 預設的工具是Pencil，可以畫出任意的線條.
2. Eraser類似Pencil，只是顏色會被設定跟canvas一樣，作用相當於擦去上面的圖形.
3. Line可以畫出直線.
4. Circle可以畫出圓形.
5. Triangle可以畫出正三角形.
6. Rectangle可以畫出長方形.
7. **Polygon可以畫出多邊形，下面的Polygon Sides是設定有幾個邊(預設七邊形)；Poly Angle可以旋轉多邊形.**
8. 可以更換canvas、線條、填滿的顏色.
9. 可以更換線條的粗細.
10. 可以輸入文字，先在Text Input欄輸入想放上去的文字，上面選取想要的大小，再按下Add就可以.
11. Clear Canvas可以清空Canvas.
12. 可以Un/Re-do.
13. 可以下載畫布的圖像.
