var canvas,
    context,
    dragging = false,
    dragStartLocation,
    text_y=70,
    snapshot=[],
    index_snapshot=0,
    max_snapshot=0;


function getCanvasCoordinates(event) {
    var x = event.clientX - canvas.getBoundingClientRect().left,
        y = event.clientY - canvas.getBoundingClientRect().top;

    return {x: x, y: y};
}

function takeSnapshot() {
    snapshot[index_snapshot] = context.getImageData(0, 0, canvas.width, canvas.height);
    max_snapshot++;
    index_snapshot++;
}

function restoreSnapshot() {
    context.putImageData(snapshot[index_snapshot-1], 0, 0);
}
function undoSnapshot(){
    if (index_snapshot == 1){
        index_snapshot = 0;
        context.putImageData(snapshot[index_snapshot], 0, 0);
    }
    else if (index_snapshot > 0){
        index_snapshot = index_snapshot -2;
        context.putImageData(snapshot[index_snapshot], 0, 0);
    }
    
}
function redoSnapshot(){
    if (index_snapshot == max_snapshot - 2){
        index_snapshot++;
        context.putImageData(snapshot[index_snapshot], 0, 0);
    }
    else if (index_snapshot < max_snapshot - 2){
        index_snapshot = index_snapshot + 2;
        context.putImageData(snapshot[index_snapshot], 0, 0);
    }
    
}
function addText(){
    var text = document.getElementById("text_input").value,
        size = document.querySelector('input[type="radio"][name="type_size"]:checked').value;
    if (size === "big")
        context.font = "90px Arial";
    else if (size === "middle")
        context.font = "60px Arial";
    else
        context.font = "20px Arial";
    context.fillText(text, 10, text_y);
    text_y = text_y + 70;
    
}
function drawPencil(position){
    context.lineTo(position.x, position.y);
    context.stroke();
}
function drawLine(position) {
    context.beginPath();
    context.moveTo(dragStartLocation.x, dragStartLocation.y);
    context.lineTo(position.x, position.y);
    context.stroke();
}

function drawCircle(position) {
    var radius = Math.sqrt(Math.pow((dragStartLocation.x - position.x), 2) + Math.pow((dragStartLocation.y - position.y), 2));
    context.beginPath();
    context.arc(dragStartLocation.x, dragStartLocation.y, radius, 0, 2 * Math.PI, false);
}

function drawPolygon(position, sides, angle) {
    var coordinates = [],
        radius = Math.sqrt(Math.pow((dragStartLocation.x - position.x), 2) + Math.pow((dragStartLocation.y - position.y), 2)),
        index = 0;

    for (index = 0; index < sides; index++) {
        coordinates.push({x: dragStartLocation.x + radius * Math.cos(angle), y: dragStartLocation.y - radius * Math.sin(angle)});
        angle += (2 * Math.PI) / sides;
    }

    context.beginPath();
    context.moveTo(coordinates[0].x, coordinates[0].y);
    for (index = 1; index < sides; index++) {
        context.lineTo(coordinates[index].x, coordinates[index].y);
    }

    context.closePath();
}
function drawRectangle(position){
    var coordinates = [],
        index = 0;

    coordinates.push({x: dragStartLocation.x, y: dragStartLocation.y });
    coordinates.push({x: dragStartLocation.x, y: position.y });
    coordinates.push({x: position.x, y: position.y });
    coordinates.push({x: position.x, y: dragStartLocation.y });

    context.beginPath();
    context.moveTo(coordinates[0].x, coordinates[0].y);
    for (index = 1; index < 4; index++) {
        context.lineTo(coordinates[index].x, coordinates[index].y);
    }
    context.closePath();
}

function draw(position) {

    var shape = document.querySelector('input[type="radio"][name="shape"]:checked').value,
        polygonSides = document.getElementById("polygonSides").value,
        polygonAngle = document.getElementById("polygonAngle").value;


    if (shape === "pencil"){
        drawPencil(position);
    }
    if (shape === "eraser"){
        drawPencil(position);
    }
    if (shape === "circle") {
        drawCircle(position);
    }
    if (shape === "line") {
        drawLine(position);
    }
    if (shape === "rectangle"){
        drawRectangle(position);
    }
    if (shape === "triangle"){
        drawPolygon(position, 3, 90 * (Math.PI / 180));
    }
    if (shape === "polygon") {
        drawPolygon(position, polygonSides, polygonAngle * (Math.PI / 180));
    }

    if (shape !== "line" && shape !== "pencil" && shape !== "eraser") {
        context.fill();
    }
}

function dragStart(event) {
    var shape = document.querySelector('input[type="radio"][name="shape"]:checked').value,
        position;
    dragging = true;
    dragStartLocation = getCanvasCoordinates(event);
    position = dragStartLocation;
    takeSnapshot();
    if (shape === "eraser"){
        context.strokeStyle = document.getElementById("backgroundColor").value;
        

    }
    if (shape === "pencil" || shape === "eraser"){
        context.beginPath();
        context.moveTo(dragStartLocation.x, dragStartLocation.y);
        //draw(position);
    }
   
}

function drag(event) {
    var position,
    shape = document.querySelector('input[type="radio"][name="shape"]:checked').value;
    
    if (shape === "pencil")
    {
        canvas.style.cursor = "url(pencil.cur) ,auto";
    }
    else if (shape === "eraser")
    {
        canvas.style.cursor = "url(eraser.png) ,auto";
    }
    else if (shape === "line"){
        canvas.style.cursor = "url(line.png) ,auto";
    }
    else if (shape === "circle"){
        canvas.style.cursor = "url(circle.png) ,auto";
    }
    else if (shape === "triangle"){
        canvas.style.cursor = "url(triangle.png) ,auto";
    }
    else if (shape === "rectangle"){
        canvas.style.cursor = "url(rectangle.png) ,auto";
    }
    else if (shape === "polygon"){
        canvas.style.cursor = "url(polygon.png) ,auto";
    }
    if (dragging === true && ( shape === "pencil" || shape === "eraser") ){
        position = getCanvasCoordinates(event);
        //takeSnapshot();
        draw(position);
    }
    else if (dragging === true) {
        restoreSnapshot();
        position = getCanvasCoordinates(event);
        draw(position);
    }
}

function dragStop(event) {
    var shape = document.querySelector('input[type="radio"][name="shape"]:checked').value;
    dragging = false;
    restoreSnapshot();
    var position = getCanvasCoordinates(event);
    draw(position);
    takeSnapshot();
    //index_snapshot--;
    if (shape === "eraser"){
        context.strokeStyle = document.getElementById("strokeColor").value;
        
    }
    
}

function changeLineWidth() {
    context.lineWidth = this.value;
    event.stopPropagation();
}

function changeFillStyle() {
    context.fillStyle = this.value;
    event.stopPropagation();
}

function changeStrokeStyle() {
    context.strokeStyle = this.value;
    event.stopPropagation();
}

function changeBackgroundColor() {
    context.save();
    context.fillStyle = document.getElementById("backgroundColor").value;
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.restore();
}

function eraseCanvas() {
    var fillColor = document.getElementById("fillColor");
    takeSnapshot();
    context.fillStyle = document.getElementById("backgroundColor").value;
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = fillColor.value;
    takeSnapshot();
    text_y = 70;
}

function init() {
    canvas = document.getElementById("canvas");
    context = canvas.getContext('2d');
    var lineWidth = document.getElementById("lineWidth"),
        fillColor = document.getElementById("fillColor"),
        strokeColor = document.getElementById("strokeColor"),
        canvasColor = document.getElementById("backgroundColor"),
        text_add = document.getElementById("text_add"),
        clearCanvas = document.getElementById("clearCanvas"),
        undo = document.getElementById("undo"),
        redo = document.getElementById("redo");

    context.fillStyle = document.getElementById("backgroundColor").value;
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.strokeStyle = strokeColor.value;
    context.fillStyle = fillColor.value;
    context.lineWidth = lineWidth.value;
    canvas.style.cursor = "url(pencil.cur) ,auto";
    

    //document.body.style.cursor=url('pencil.cur'),pointer;

    canvas.addEventListener('mousedown', dragStart, false);
    canvas.addEventListener('mousemove', drag, false);
    canvas.addEventListener('mouseup', dragStop, false);
    lineWidth.addEventListener("input", changeLineWidth, false);
    fillColor.addEventListener("input", changeFillStyle, false);
    strokeColor.addEventListener("input", changeStrokeStyle, false);
    canvasColor.addEventListener("input", changeBackgroundColor, false);
    text_add.addEventListener("click", addText, false);
    clearCanvas.addEventListener("click", eraseCanvas, false);
    undo.addEventListener("click", undoSnapshot, false);
    redo.addEventListener("click", redoSnapshot, false);

    document.getElementById('download').addEventListener('click', function() {
        var base64Img = canvas.toDataURL();
        var oA = document.createElement('a');
        oA.href = base64Img;
        oA.download = 'canvas.png';
    
        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        oA.dispatchEvent(event);
    });
}

window.addEventListener('load', init, false);